@Basic
@Lob
@Entity(name = "User_Details") and @Table(name = "User_Details") - @Entity mean you are changing the entity name and same will use in HQL.
																   @Table mean you are giving explicitly table name.																   
@Temporal - Its used to define strategy to store either only Date or Time.
@Embadable - Its used on Entity level i'e the entity eligible to inject other entity.
@Embaded - Its used to add an entity in another entity for example User class has Address class.
@AttributeOverrides(
	@AttributeOverride(name = "street", column=@Column(name="HOME_STREET_NAME"))
	@AttributeOverride(name = "city", column=@Column(name="HOME_CITY_NAME"))
)
@EmbadedId - Its used to used primaryKey from other table

@ElementCollection - It will create separate table and record Address.  Here you can use fetch = FetchType.EAGER or LAZY
@JoinTable(name="User_Address", joinColumns=@JoinColumn(name="USER_ID")) - Its used to give collections table name i'e collections of Addresses.
private Set<Address> listOfAddresses = new HashSet<Address>();

@ElementCollection - It will create separate table and record Address. Here you can use fetch = FetchType.EAGER or LAZY
@JoinTable(name="User_Address", joinColumns=@JoinColumn(name="USER_ID")) - Its used to give collections table name i'e collections of Addresses.
@GenericGenerator(name="hilo=gen", strategy="hilo") - Its used to generate id in sub-table
@CollectionId(columns = {@Column(name = "ADDRESS_ID")}, generator="hilo-gen", type = @Type(type="long")) - Its used for own primaryKey in sub-table.
private List<Address> listOfAddresses = new ArrayList<Address>();

@Inheritance(strategy = InheritanceType.SINGLE_TABLE) - Its default behaviour of hibernate.




